﻿insert into Usuario values(0,'default','default', 'default' ,'default');

insert into Tipo values(0,'Fondo');
insert into Tipo values(0,'Objeto');
insert into Tipo values(0,'Carta');

-inserts para los Imagenes-
insert into Imagenes values(0,1,1, 'images/fondo2.jpeg' ,'fondo de la orilla del mar');
insert into Imagenes values(0,1,1, 'images/fondo3.jpeg' ,'fondo rojo con degradado');
insert into Imagenes values(0,1,1, 'images/fondo4.jpeg' ,'fondo azul con un paragüas');

-inserts para los Imagenes-
insert into Imagenes values(0,1,2 , 'images/objeto1.png' ,'jirafa');
insert into Imagenes values(0,1,2, 'images/objeto2.png','cometa con forma de pez');
insert into Imagenes values(0,1,2, 'images/objeto3.png','calabaza hallowen');
insert into Imagenes values(0,1,2 , 'images/objeto4.png','herradura');
insert into Imagenes values(0,1,2, 'images/objeto5.png','monstruo');
insert into Imagenes values(0,1,2, 'images/objeto6.png','biberón');
insert into Imagenes values(0,1,2 ,'images/objeto7.png','robot');
insert into Imagenes values(0,1,2, 'images/objeto8.png','fantasma');
insert into Imagenes values(0,1,2, 'images/objeto9.png','trompeta');
insert into Imagenes values(0,1,2 ,'images/objeto10.png','pez amarillo');
insert into Imagenes values(0,1,2, 'images/objeto12.png','carita feliz');

-inserts para las Imagenes-
insert into Imagenes values(0,1,3, 'images/carta2.jpeg','reverso carta azul y blanca');
insert into Imagenes values(0,1,3, 'images/carta3.png','reverso carta negra y símbolo azul');
insert into Imagenes values(0,1,3, 'images/carta4.png', 'reverso carta amarilla con silueta');
insert into Imagenes values(0,1,3, 'images/carta5.jpeg','reverso carta roja y marrón');
insert into Imagenes values(0,1,3, 'images/carta6.png', 'reverso carta roja con estrella');
insert into Imagenes values(0,1,3 ,'images/carta7.png', 'reverso carta de zombies');
insert into Imagenes values(0,1,3, 'images/carta8.png', 'reverso carta efecto floral');