/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var idimg = "";
var srcimg = "";
var altimg = "";
var idreverso;



function actualizarScript() {

    $(".imagenesbarra.Objeto").draggable({
        revert: "invalid",
        helper: "clone",
        drag: function(event, ui) {
            //    imgId = $(this).attr('id');


        }

    });

    $(".drop").droppable({
        tolerance: "fit",
        drop: function(event, ui) {

            $(this).find("img").remove();

            $(this).append($(ui.draggable).clone());


        }


    });

    $(".imagenesbarra.Objeto").click(function() {

        srcimg = $(this).attr("src");
        idimg = $(this).attr("id");
        altimg = $(this).attr("alt");
    });
    $(".drop").click(function() {
        if (idimg != "") {
            $(this).find("img").remove();
            $(this).append("<img class='imagenesbarra' id=" + idimg + " src='" + srcimg + "' alt='" + altimg + "'  />");
            idimg = "";
            srcimg = "";
            altimg = "";

        } else {
            idimg = $(this).find("img").attr("id");
            srcimg = $(this).find("img").attr("src");
            alt = $(this).find("img").attr("alt");

        }

    });

    $('.imagenesbarra.Fondo').click(function() {
       
        $idfondo = $(this).attr("id");
        $srcfondo = $(this).attr("src");
        
        $("#fondo").val($idfondo);
        $("#fondobtn").val($idfondo);
        $("#principal")
                .animate({opacity: 0}, 'slow', function() {
                    $(this)
                            .css({'background-image': 'url(' + $srcfondo + ')', 'background-size': '100% 100%'})
                            .animate({opacity: 1});
                });
    });
    $(".container").shapeshift();




}

function cargardatos() {
    //actualizarVarFondo();
    var divs = $(".drop > img");
    var elem = "";
    $.each(divs, function(i, val) {

        elem = elem + " " + $(val).attr("id");


    });

    $("#imagenes").val(elem);
    $("#imagenesbtn").val(elem);

}


function canvas() {
    cargardatos();
   canvasaux();
   
}
function canvasaux() {
    html2canvas($('#principal'), {
        onrendered: function(canvas) {
            var $img;
            $img = canvas.toDataURL("image/png");
            $("#imgjuego").val($img);
            
        }
    }); 
}

function cargarfb(){
    
}


//function actualizarVarFondo() {
//
//    $idfondo = $("#principal").attr("class");
//    alert($idfondo);
//    var bg = $("#principal").css('background-image');
//    bg = bg.replace('url(http://', '').replace(')', '');
//    bg.subtring(lastIndexOf('/',0), bg.lenght()); tamaño - 0
//    $srcfondo = bg;
//    alert($srcfondo);
//}


function cargardereverso() {
    if (idreverso != null) {
       $("#" + idreverso).css({'border-radius': '10px', 'border-style': 'solid', 'border-color': '#ff0000'});
   }
   


    $('.imagenesbarra.Carta').click(function() {

        if (idreverso == null) {
            var id = $(this).attr("id");
            $("#reverso").val(id);
            $("#reversobtn").val(id);
            $("#" + id).css({'border-radius': '10px', 'border-style': 'solid', 'border-color': '#ff0000'});
            idreverso = $(this).attr("id");
        }
        else {
            var id = $(this).attr("id");
            $("#" + idreverso).css({'border-radius': '0', 'border-style': 'none', 'border-color': '#000000'});
            $("#reverso").val(id);
            $("#reversobtn").val(id);
            $("#" + id).css({'border-radius': '10px', 'border-style': 'solid', 'border-color': '#ff0000'});
            idreverso = $(this).attr("id");
        }
    });
}
