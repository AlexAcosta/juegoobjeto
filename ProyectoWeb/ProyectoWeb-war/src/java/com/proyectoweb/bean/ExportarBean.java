/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectoweb.bean;

import com.proyectoweb.ejb.FaseFacade;
import com.proyectoweb.ejb.JuegoFacade;
import com.proyectoweb.ejb.RecuadrosFacade;
import com.proyectoweb.entity.Fase;
import com.proyectoweb.entity.Juego;
import com.proyectoweb.entity.Recuadros;
import com.sun.xml.xsom.impl.scd.Iterators;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.util.Base64;

/**
 *
 * @author Daniel
 */
@ManagedBean
@RequestScoped
public class ExportarBean implements Serializable {
    @EJB
    private RecuadrosFacade recuadrosFacade;

    @EJB
    private FaseFacade faseFacade;

    @EJB
    private JuegoFacade juegoFacade;

    @ManagedProperty(value = "#{faseBean}")
    private FaseBean faseFinal;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean usuarioSession;

    @ManagedProperty(value = "#{juegoBean}")
    private JuegoBean juego;

    protected boolean juegoCompleto = true;
    protected StringBuffer msgError;
    private static final Logger LOG = Logger.getLogger(ExportarBean.class.getName());

    protected String imagen;

    /**
     * Creates a new instance of ExportarBean
     */
    public ExportarBean() {
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public LoginBean getUsuarioSession() {
        return usuarioSession;
    }

    public void setUsuarioSession(LoginBean usuarioSession) {
        this.usuarioSession = usuarioSession;
    }

    public StringBuffer getMsgError() {
        return msgError;
    }

    public void setMsgError(StringBuffer msgError) {
        this.msgError = msgError;
    }

    public JuegoBean getJuego() {
        return juego;
    }

    public void setJuego(JuegoBean juego) {
        this.juego = juego;
    }

    public boolean isJuegoCompleto() {
        return juegoCompleto;
    }

    public void setJuegoCompleto(boolean juegoCompleto) {
        this.juegoCompleto = juegoCompleto;
    }

    public FaseBean getFaseFinal() {
        return faseFinal;
    }

    public void setFaseFinal(FaseBean faseFinal) {
        this.faseFinal = faseFinal;
    }

    public void guardarImagenFin() throws FileNotFoundException, IOException {
        String img = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("imgjuego");
        LOG.info("img = " + img);
        LOG.info("imagen = " + imagen);
        String imgfinal;
        imgfinal = img.replace("data:image/png;base64,", "");

        byte[] imageByteArray = Base64.decode(imgfinal);
        LOG.info("imageByteArray = " + imageByteArray + " - tamano = " + imageByteArray.length);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        String path = externalContext.getRealPath("/");
        path = path.substring(0, path.indexOf("dist")) + "ProyectoWeb-war/web/images/";
        // Write a image byte array into file system
        String direccion = path + "/" + usuarioSession.usuario + juego.juegoActual.getIdnombre().getNombre() + ".jpg";
        File fileTemp = new File(direccion);
        if (fileTemp.exists()) {
            fileTemp.delete();
        }
        fileTemp.createNewFile();
        FileOutputStream imageOutFile = new FileOutputStream(fileTemp);
//        FileOutputStream imageOutFile = new FileOutputStream(path + "/"+usuarioSession.usuario+juego.juegoActual.getIdnombre().getNombre()+".jpg");

        imageOutFile.write(imageByteArray);

        imageOutFile.flush();
        imageOutFile.close();

       
    }

    public void exportarJuego() throws IOException {
        this.juegoCompleto = true;
        int i;

        faseFinal.guardar();
        List<Fase> fases = this.faseFacade.obtenerFasesUsuario(juego.juegoActual.getIdjuego(), usuarioSession.usuarioActual.getIdusuario());
        List<Boolean> fasesErroneas = new ArrayList<>();

        List<Recuadros> listRecuadros;

        for (i = 0; i < fases.size(); i++) {
            Fase f = fases.get(i);
            listRecuadros = recuadrosFacade.obtenerRecuadrosFase(fases.get(i).getIdfase());

            int contImg = 0;

            BigDecimal idObjetoFijo = listRecuadros.get(0).getIdimagenes().getIdimagen();
            BigDecimal codigoImgBlanco = BigDecimal.valueOf(42);
            if (idObjetoFijo.equals(codigoImgBlanco)) {
                contImg++;
            }

            int contCopias = 0;

            for (int j = 1; j < listRecuadros.size(); j++) {
                BigDecimal idObjetoRecuadro = listRecuadros.get(j).getIdimagenes().getIdimagen();

                if (idObjetoRecuadro.equals(idObjetoFijo)) {
                    contCopias++;
                }

                if (idObjetoRecuadro.equals(codigoImgBlanco)) {
                    contImg++;
                }
            }

            if (contCopias == 0 || (contImg != 0)) {
                this.juegoCompleto = false;
                fasesErroneas.add(false);
               
            } else {
                fasesErroneas.add(true);
               
            }
        }

        if (this.juegoCompleto == false) {

            msgError = new StringBuffer("ERROR EN FASE: ");
            for (i = 0; i < fasesErroneas.size(); i++) {
                if (fasesErroneas.get(i).booleanValue() == false) {
                    this.msgError.append(i + 1 + " ");
                }
            }
      
            Juego j = juegoFacade.find(juego.juegoActual.getIdjuego());
            j.setTerminado(0);
            juegoFacade.edit(j);
        } else {
            Juego j = juegoFacade.find(juego.juegoActual.getIdjuego());
            j.setTerminado(1);
            j.setDireccionimagen( "images/"+usuarioSession.usuario + juego.juegoActual.getIdnombre().getNombre() + ".jpg");
            juegoFacade.edit(j);
            guardarImagenFin();
           
        }

    }

}
