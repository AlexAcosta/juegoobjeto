package com.proyectoweb.bean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.proyectoweb.ejb.JuegoFacade;
import com.proyectoweb.ejb.NombrejuegoFacade;
import com.proyectoweb.entity.Juego;
import com.proyectoweb.entity.Nombrejuego;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.util.Base64;

@ManagedBean
@SessionScoped
public class JuegoBean implements Serializable {

    @EJB
    private JuegoFacade juegoFacade;
    @EJB
    private NombrejuegoFacade nombrejuegoFacade;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean usuarioSession;

    private int pista;
    private int accionobjeto;
    protected String juego;
    protected Juego juegoActual;

    public JuegoBean() {
    }

    public Juego getJuegoActual() {
        return juegoActual;
    }

    public void setJuegoActual(Juego juegoActual) {
        this.juegoActual = juegoActual;
    }

    public LoginBean getUsuarioSession() {
        return usuarioSession;
    }

    public void setUsuarioSession(LoginBean usuarioSession) {
        this.usuarioSession = usuarioSession;
    }

    public String getJuego() {
        return juego;
    }

    public void setJuego(String juego) {
        this.juego = juego;
    }

    public int getPista() {
        return pista;
    }

    public void setPista(int pista) {
        this.pista = pista;
    }

    public int getAccionobjeto() {
        return accionobjeto;
    }

    public void setAccionobjeto(int accionobjeto) {
        this.accionobjeto = accionobjeto;
    }

    public String cargarJuegoCartas() {
        if (usuarioSession.logeado) {
            this.juego = "Cartas";
            this.juegoActual = this.juegoFacade.obtenerJuegoPorUsuarioYJuego(usuarioSession.usuarioActual, this.nombrejuegoFacade.obtenerNombrejuegoPorID(juego));
            this.pista = juegoActual.getPista();
            this.accionobjeto = juegoActual.getAccionobjeto();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("faseBean");
                 return "juegoCartas.xhtml";
        } else {
            return "inicio.xhtml";
        }

    }

    public String cargarJuegoObjetos() {
        if (usuarioSession.logeado) {
            this.juego = "Objetos";
            this.juegoActual = this.juegoFacade.obtenerJuegoPorUsuarioYJuego(usuarioSession.usuarioActual, this.nombrejuegoFacade.obtenerNombrejuegoPorID(juego));
            this.pista = juegoActual.getPista();
            this.accionobjeto = juegoActual.getAccionobjeto();
          FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("faseBean");
          
                   return "juegoObjetos.xhtml";
        } else {
            return "inicio.xhtml";
        }
    }

    public void guardarJuego() {
        juegoActual.setPista(pista);
        juegoActual.setAccionobjeto(accionobjeto);
        this.juegoFacade.edit(juegoActual);
    }

    @PostConstruct
    public void cargar() {
        this.pista = 1;
        this.accionobjeto = 1;
        this.juego = "Cartas";
    }
}
