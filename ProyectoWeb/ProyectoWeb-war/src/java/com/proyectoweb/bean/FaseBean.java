package com.proyectoweb.bean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.proyectoweb.ejb.FaseFacade;
import com.proyectoweb.ejb.ImagenesFacade;
import com.proyectoweb.ejb.RecuadrosFacade;
import com.proyectoweb.ejb.UsuarioFacade;
import com.proyectoweb.entity.Fase;
import com.proyectoweb.entity.Imagenes;
import com.proyectoweb.entity.Recuadros;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author nacho_mc15
 */

@ManagedBean
@SessionScoped
public class FaseBean implements Serializable {

    @EJB
    private RecuadrosFacade recuadrosFacade;
    @EJB
    private ImagenesFacade imagenesFacade;
    @EJB
    private FaseFacade faseFacade;
    @EJB
    private UsuarioFacade usuarioFacade;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean usuarioSession;

    @ManagedProperty(value = "#{juegoBean}")
    private JuegoBean juego;

    private int numFases;
    private int faseActual;
    private int tiempo;
    private int numIntentos;
    private int numObjetos;
    private int numCartas;
    protected List<Fase> fases;
    protected List<Recuadros> recuadrosActual;
    protected Recuadros recuadroFijo;

    private String faseSeleccionada;
    private String faseAnteriorSeleccionada;
    private static final Logger LOG = Logger.getLogger(FaseBean.class.getName());

    public FaseBean() {

    }

    public Recuadros getRecuadroFijo() {
        return recuadroFijo;
    }

    public void setRecuadroFijo(Recuadros recuadroFijo) {
        this.recuadroFijo = recuadroFijo;
    }

    public List<Recuadros> getRecuadrosActual() {
        return recuadrosActual;
    }

    public void setRecuadrosActual(List<Recuadros> recuadrosActual) {
        this.recuadrosActual = recuadrosActual;
    }

    public LoginBean getUsuarioSession() {
        return usuarioSession;
    }

    public void setUsuarioSession(LoginBean usuarioSession) {
        this.usuarioSession = usuarioSession;
    }

    public JuegoBean getJuego() {
        return juego;
    }

    public void setJuego(JuegoBean juego) {
        this.juego = juego;
    }

    public int getNumCartas() {
        return numCartas;
    }

    public void setNumCartas(int numCartas) {
        this.numCartas = numCartas;
    }

    public int getNumObjetos() {
        return numObjetos;
    }

    public void setNumObjetos(int numObjetos) {
        this.numObjetos = numObjetos;
    }

    public String getFaseAnteriorSeleccionada() {
        return faseAnteriorSeleccionada;
    }

    public void setFaseAnteriorSeleccionada(String faseAnteriorSeleccionada) {
        this.faseAnteriorSeleccionada = faseAnteriorSeleccionada;
    }

    public String getFaseSeleccionada() {
        return faseSeleccionada;
    }

    public void setFaseSeleccionada(String faseSeleccionada) {
        this.faseSeleccionada = faseSeleccionada;
    }

    public List<Fase> getFases() {
        return fases;
    }

    public void setFases(List<Fase> fases) {
        this.fases = fases;
    }

    public int getNumFases() {
        return numFases;
    }

    public void setNumFases(int numFases) {
        this.numFases = numFases;
    }

    public int getFaseActual() {
        return faseActual;
    }

    public void setFaseActual(int faseActual) {
        this.faseActual = faseActual;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getNumIntentos() {
        return numIntentos;
    }

    public void setNumIntentos(int numIntentos) {
        this.numIntentos = numIntentos;
    }

    public void cargarNumFases() {
        if (numFases != 0) {
            List<Fase> l = new ArrayList<>();
            //Creamos una lista del tamaño dado inicializada a los valores por defecto
            for (int i = 1; i <= numFases; i++) {
                Fase fase = new Fase();
                fase.setIdfase(new BigDecimal(0));
                fase.setIdusuario(this.usuarioSession.usuarioActual);
                fase.setNintento(10);
                fase.setTiempo(10);
                fase.setIdjuego(juego.getJuegoActual());

                switch (juego.juego) {
                    case "Cartas":
                        fase.setNobjeto(4);

                        break;
                    case "Objetos":
                        fase.setNobjeto(3);

                        break;
                }
                List<Recuadros> aux = new ArrayList<>();
                for (int j = 0; j < fase.getNobjeto() + 1; j++) {
                    Recuadros r = new Recuadros();

                    r.setOrden(BigInteger.valueOf(j + 1));
                    Imagenes img = imagenesFacade.obtenerImagenesPorID(BigDecimal.valueOf(42));

                    r.setIdimagenes(img);

                    aux.add(r);
                }
                this.recuadrosActual = aux;
                fase.setRecuadrosList(recuadrosActual);

                fase.setNombre("Fase " + i);
                l.add(fase);
            }
            if (!fases.isEmpty()) {
                if (fases.size() < l.size()) {
                    l = l.subList(fases.size(), l.size());
                    this.fases.addAll(l);
                } else {
                    List<Fase> fasesABorrar = fases.subList(numFases, fases.size());
                    this.fases = fases.subList(0, numFases);
                    for (Fase f : fasesABorrar) {
                        Fase f2 = this.faseFacade.obtenerFase(f.getIdjuego().getIdjuego(), f.getNombre(), f.getIdusuario().getIdusuario());
                        this.faseFacade.remove(f2);
                    }
                    this.tiempo = fases.get(0).getTiempo();
                    this.numIntentos = fases.get(0).getNintento();
                    this.numObjetos = fases.get(0).getNobjeto();
                    this.numCartas = fases.get(0).getNobjeto();
                    recuadrosActual = fases.get(0).getRecuadrosList();

                }
            } else {
                fases = l;
                this.faseAnteriorSeleccionada = fases.get(0).getNombre();
                this.faseSeleccionada = fases.get(0).getNombre();
            }
        }
    }

    public void guardarDatosFase() {
        //Guardamos los datos anteriores 
        if(numFases!=0){
        if (faseActual < fases.size()) {
            this.faseActual = Integer.parseInt(faseAnteriorSeleccionada.substring(faseAnteriorSeleccionada.length() - 1)) - 1;
            String img = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap().get("imagenes");
            List<String> listaIdImagenes = new ArrayList<String>(Arrays.asList(img.split(" ")));
            String idfondo = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap().get("fondo");
            String idreverso = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap().get("reverso");
            Imagenes fondo = null;
            if (!idfondo.equals("")) {
                fondo = imagenesFacade.obtenerImagenesPorID(BigDecimal.valueOf(Double.valueOf(idfondo)));
            }
            Imagenes reverso = null;

            if (idreverso != null) {
                if (!idreverso.equals("")) {
                    reverso = imagenesFacade.obtenerImagenesPorID(BigDecimal.valueOf(Double.valueOf(idreverso)));
                }
            }
            List<Recuadros> aux = new ArrayList<>();

            for (int i = 1; i < listaIdImagenes.size(); i++) {
                Recuadros r = new Recuadros();
                r.setOrden(BigInteger.valueOf(i));
                r.setIdimagenes(imagenesFacade.obtenerImagenesPorID(BigDecimal.valueOf(Double.valueOf(listaIdImagenes.get(i)))));

                aux.add(r);
            }
            fases.get(faseActual).setRecuadrosList(aux);
            fases.get(faseActual).setTiempo(tiempo);
            fases.get(faseActual).setNintento(numIntentos);
            if (!idfondo.equals("")) {
                fases.get(faseActual).setIdfondo(fondo);
            }
            if (idreverso != null) {
                if (!idreverso.equals("")) {
                    fases.get(faseActual).setIdtipocarta(reverso);
                }
            }

            switch (juego.juego) {
                case "Cartas":
                    fases.get(faseActual).setNobjeto(numCartas);
                    break;
                case "Objetos":
                    fases.get(faseActual).setNobjeto(numObjetos);
                    break;
            }
        }
        //actualizamos la fase
        faseAnteriorSeleccionada = faseSeleccionada;
        //Cargamos los datos de la fase actual
        this.faseActual = Integer.parseInt(faseSeleccionada.substring(faseSeleccionada.length() - 1)) - 1;
        this.tiempo = fases.get(faseActual).getTiempo();
        this.numIntentos = fases.get(faseActual).getNintento();
        this.numCartas = fases.get(faseActual).getNobjeto();
        this.numObjetos = fases.get(faseActual).getNobjeto();

        this.recuadrosActual = fases.get(faseActual).getRecuadrosList();
        this.recuadroFijo = recuadrosActual.get(0);
    }
    }

    public void actualizarObjetos() {
        if(numFases!=0){
        //HACER LO MISMO CON RECUADROSACTUAL
        List<Recuadros> l = new ArrayList<>();

        for (int i = 0; i < this.numObjetos + 1; i++) {
            Recuadros r = new Recuadros();
            r.setOrden(BigInteger.valueOf(i));
            Imagenes img = new Imagenes();
            img.setIdimagen(BigDecimal.valueOf(42));
            img.setDescripcion("Hueco en blanco");
            img.setDireccion("images/blanco.png");
            r.setIdimagenes(img);
            l.add(r);
        }
        if (recuadrosActual.size() < l.size()) {
            l = l.subList(recuadrosActual.size(), l.size());
            this.recuadrosActual.addAll(l);
        } else {
            List<Recuadros> recuadrosABorrar = new ArrayList<>();
            recuadrosABorrar = recuadrosActual.subList(numObjetos + 1, recuadrosActual.size());
            this.recuadrosActual = recuadrosActual.subList(0, numObjetos + 1);

            for (Recuadros r : recuadrosABorrar) {
                Recuadros borrar = recuadrosFacade.obtenerRecuadro(fases.get(faseActual).getIdfase(), r.getOrden());
                if (borrar != null) {
                    recuadrosFacade.remove(borrar);
                }
            }

        }
        fases.get(faseActual).setRecuadrosList(recuadrosActual);
        }
    }

    public void actualizarCartas() {
        if(numFases!=0){
        //HACER LO MISMO CON RECUADROSACTUAL
        List<Recuadros> l = new ArrayList<>();

        for (int i = 0; i < this.numCartas; i++) {
            Recuadros r = new Recuadros();
            r.setOrden(BigInteger.valueOf(i));
            Imagenes img = new Imagenes();
            img.setIdimagen(BigDecimal.valueOf(42));
            img.setDescripcion("Hueco en blanco");
            img.setDireccion("images/blanco.png");
            r.setIdimagenes(img);
            l.add(r);
        }
        if (recuadrosActual.size() < l.size()) {
            l = l.subList(recuadrosActual.size(), l.size());
            this.recuadrosActual.addAll(l);
        } else {
            List<Recuadros> recuadrosABorrar = new ArrayList<>();
            recuadrosABorrar = recuadrosActual.subList(numCartas, recuadrosActual.size());
            this.recuadrosActual = recuadrosActual.subList(0, numCartas);

            for (Recuadros r : recuadrosABorrar) {
                Recuadros borrar = recuadrosFacade.obtenerRecuadro(fases.get(faseActual).getIdfase(), r.getOrden());
                if (borrar != null) {
                    recuadrosFacade.remove(borrar);
                }
            }

        }
        fases.get(faseActual).setRecuadrosList(recuadrosActual);
    }
    }

    public void guardar() {
        if(numFases!=0){
        this.faseActual = Integer.parseInt(faseSeleccionada.substring(faseSeleccionada.length() - 1)) - 1;

        String img = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap().get("imagenesbtn");
        List<String> listaIdImagenes = new ArrayList<String>(Arrays.asList(img.split(" ")));
        String idfondo = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap().get("fondobtn");
        String idreverso = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap().get("reversobtn");
        Imagenes fondo = null;
        if (!idfondo.equals("")) {
            fondo = imagenesFacade.obtenerImagenesPorID(BigDecimal.valueOf(Double.valueOf(idfondo)));
        }
        Imagenes reverso = null;
        if (idreverso != null) {
            if (!idreverso.equals("")) {
                reverso = imagenesFacade.obtenerImagenesPorID(BigDecimal.valueOf(Double.valueOf(idreverso)));
            }
        }

        List<Recuadros> aux = new ArrayList<>();
        for (int i = 1; i < listaIdImagenes.size(); i++) {
            Recuadros r = new Recuadros();
            r.setOrden(BigInteger.valueOf(i));
            r.setIdimagenes(imagenesFacade.obtenerImagenesPorID(BigDecimal.valueOf(Double.valueOf(listaIdImagenes.get(i)))));

            aux.add(r);
        }
        recuadrosActual = aux;
        fases.get(faseActual).setRecuadrosList(recuadrosActual);
        fases.get(faseActual).setTiempo(tiempo);
        fases.get(faseActual).setNintento(numIntentos);
        if (!idfondo.equals("")) {
            fases.get(faseActual).setIdfondo(fondo);
        }
        if (idreverso != null) {
            if (!idreverso.equals("")) {
                fases.get(faseActual).setIdtipocarta(reverso);
            }
        }
        switch (juego.juego) {
            case "Cartas":
                fases.get(faseActual).setNobjeto(numCartas);
                break;
            case "Objetos":
                fases.get(faseActual).setNobjeto(numObjetos);
                break;
        }
        List<Recuadros> aux2 = new ArrayList<>();
        for (Fase f : fases) {
            Fase f2 = this.faseFacade.obtenerFase(f.getIdjuego().getIdjuego(), f.getNombre(), f.getIdusuario().getIdusuario());
            aux2 = f.getRecuadrosList();
            if (f2 == null) {

                f.setRecuadrosList(null);
                this.faseFacade.create(f);
                f.setRecuadrosList(aux2);
            } else {
                f2.setTiempo(f.getTiempo());
                f2.setNobjeto(f.getNobjeto());
                f2.setNintento(f.getNintento());
                f2.setIdfondo(f.getIdfondo());
                f2.setIdtipocarta(f.getIdtipocarta());
                this.faseFacade.edit(f2);
            }
            f2 = this.faseFacade.obtenerFase(f.getIdjuego().getIdjuego(), f.getNombre(), f.getIdusuario().getIdusuario());

            for (Recuadros r : aux2) {

                r.setIdfase(f2);
                r.setIdrecuadro(BigDecimal.ZERO);
                Recuadros r2 = recuadrosFacade.obtenerRecuadro(f2.getIdfase(), r.getOrden());
                if (r2 == null) {
                    recuadrosFacade.create(r);
                } else {
                    r2.setIdimagenes(r.getIdimagenes());
                    recuadrosFacade.edit(r2);
                }

            }
        }
    }
    }

    @PostConstruct
    public void cargar() {
        fases = new ArrayList<>();

        recuadrosActual = new ArrayList<>();

        // List<Recuadros> list = new ArrayList<>();
        this.fases = this.faseFacade.obtenerFasesUsuario(juego.juegoActual.getIdjuego(), usuarioSession.usuarioActual.getIdusuario());

        if (fases.isEmpty()) {
            this.numFases = 0;
            this.tiempo = 10;
            this.numIntentos = 10;
            this.numCartas = 4;
            this.numObjetos = 3;

            for (int i = 0; i < numObjetos + 1; i++) {
                Recuadros r = new Recuadros();

                r.setOrden(BigInteger.valueOf(i));
                Imagenes img = imagenesFacade.obtenerImagenesPorID(BigDecimal.valueOf(42));

                r.setIdimagenes(img);

                recuadrosActual.add(r);
            }
            recuadroFijo = recuadrosActual.get(0);

        } else {
            this.faseAnteriorSeleccionada = "Fase 1";
            this.faseSeleccionada = "Fase 1";
            this.numFases = fases.size();
            this.tiempo = fases.get(0).getTiempo();
            this.numIntentos = fases.get(0).getNintento();
            fases.get(0).setRecuadrosList(recuadrosFacade.obtenerRecuadrosFase(fases.get(0).getIdfase()));
            recuadrosActual = fases.get(0).getRecuadrosList();

            recuadroFijo = recuadrosActual.get(0);
            // recuadrosActual = recuadrosActual.subList(1, list.size());

            switch (juego.juego) {
                case "Cartas":
                    this.numCartas = fases.get(0).getNobjeto();
                    break;
                case "Objetos":
                    this.numObjetos = fases.get(0).getNobjeto();
                    break;
            }
        }

    }
}
