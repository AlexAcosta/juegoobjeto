/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyectoweb.bean;

import com.proyectoweb.ejb.JuegoFacade;
import com.proyectoweb.ejb.NombrejuegoFacade;
import com.proyectoweb.ejb.UsuarioFacade;
import com.proyectoweb.entity.Juego;
import com.proyectoweb.entity.Nombrejuego;
import com.proyectoweb.entity.Usuario;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ninus
 */
@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {

    @EJB
    private NombrejuegoFacade nombrejuegoFacade;
    @EJB
    private JuegoFacade juegoFacade;
    @EJB
    private UsuarioFacade usuarioFacade;

    @NotNull
    @Size(min = 1, max = 20)
    protected String nombre;
    @NotNull
    @Size(min = 1, max = 20)
    protected String apellidos;
    @NotNull
    @Size(min = 4, max = 30)
    protected String pwd;
    @NotNull
    @Size(min = 4, max = 20)
    protected String confirmarPwr;
    @NotNull
    @Size(min = 1, max = 10)
    protected String usuario;

    protected String email;
    protected boolean logeado;
    protected Usuario usuarioActual;

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public Usuario getUsuarioActual() {
        return usuarioActual;
    }

    public void setUsuarioActual(Usuario usuarioActual) {
        this.usuarioActual = usuarioActual;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmarPwr() {
        return confirmarPwr;
    }

    public void setConfirmarPwr(String confirmarPwr) {
        this.confirmarPwr = confirmarPwr;
    }

    public boolean isLogeado() {
        return logeado;
    }

    public void setLogeado(boolean logeado) {
        this.logeado = logeado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String registrar() {
        Usuario user = new Usuario();
        Usuario existe = usuarioFacade.findByLogin(usuario);

        if (existe == null) {
            if (pwd.compareTo(confirmarPwr) == 0) {
                user.setApellido(apellidos);
                user.setLogin(usuario);
                user.setNombre(nombre);
                user.setPass(pwd);
                user.setCorreo(email);
                user.setIdusuario(BigDecimal.ZERO);
                usuarioFacade.create(user);
                user = usuarioFacade.findByLoginAndPasswd(usuario, pwd);
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().getSessionMap().put("user", user);
                logeado = true;
                boolean logout = true;
                context.getExternalContext().getSessionMap().put("logout", logout);
                this.usuarioActual = user;
                Juego game = new Juego();

                game.setIdusuario(user);
                game.setPista(1);
                game.setIdjuego(BigDecimal.ZERO);
                List<Nombrejuego> tipo = nombrejuegoFacade.findAll();
                game.setIdnombre(tipo.get(0));
                game.setTerminado(0);
                game.setAccionobjeto(1);
                juegoFacade.create(game);
                Juego j = new Juego();
                j.setIdusuario(user);
                j.setPista(1);
                j.setAccionobjeto(1);
                j.setTerminado(0);
                j.setIdjuego(BigDecimal.ZERO);
                j.setIdnombre(tipo.get(1));
                juegoFacade.create(j);
            } else {
                FacesMessage msg = new FacesMessage();
                msg.setSummary("Las contraseñas no coinciden");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return "registro";
            }
        } else {
            FacesMessage msg = new FacesMessage();
            msg.setSummary("Existe el nombre usuario");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return "registro";
        }
        FacesMessage msg = new FacesMessage();
        msg.setSummary("Registrado con existo ");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "inicio";
    }

    public void comprobar() {

        Usuario user = new Usuario();
        user = usuarioFacade.findByLoginAndPasswd(usuario, pwd);
        if (user != null) {
            apellidos = user.getApellido();
            nombre = user.getNombre();
            email = user.getCorreo();
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().put("user", user);
            logeado = true;
            boolean logout = true;
            context.getExternalContext().getSessionMap().put("logout", logout);
            this.usuarioActual = user;

        } else {
            FacesMessage msg = new FacesMessage();
            msg.setSummary("Usuario o Contraseña incorrectos");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            logeado = false;
        }
    }

    public void editar() {
        Usuario user = new Usuario();
        FacesContext context = FacesContext.getCurrentInstance();
        user = (Usuario) context.getExternalContext().getSessionMap().get("user");
        user.setApellido(apellidos);
        user.setLogin(usuario);
        user.setNombre(nombre);
        user.setPass(pwd);
        user.setCorreo(email);
        usuarioFacade.edit(user);
        FacesMessage confirmacion = new FacesMessage();
        confirmacion.setSummary("Usuario Modificado");
        confirmacion.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, confirmacion);

    }

    public String perfil() {

        if (logeado == true) {
            return "perfil.xhtml";
        }
        return "registro.xhtml";
    }

    public String logout() {
        FacesContext cont = FacesContext.getCurrentInstance();
        cont.getExternalContext().getSessionMap().clear();
        this.usuarioActual = null;
        return "inicio";
    }

}
