package com.proyectoweb.bean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.proyectoweb.ejb.ImagenesFacade;
import com.proyectoweb.ejb.TipoFacade;
import com.proyectoweb.entity.Imagenes;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FileUtils;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author nacho_mc15
 */
@ManagedBean
@SessionScoped
public class OpcionesBean implements Serializable{
    @EJB
    private TipoFacade tipoFacade;
    @EJB
    private ImagenesFacade imagenesFacade;

    @ManagedProperty(value="#{loginBean}")
    private LoginBean usuarioSession;
    
    private UploadedFile imagenACargar;
    private List<Imagenes> lista;
    private String opcionSeleccionada;
    private String message="";
    private List<String> imagenesseleccionadas;

    public LoginBean getUsuarioSession() {
        return usuarioSession;
    }

    public void setUsuarioSession(LoginBean usuarioSession) {
        this.usuarioSession = usuarioSession;
    }

    public List<String> getImagenesseleccionadas() {
        return imagenesseleccionadas;
    }

    public void setImagenesseleccionadas(List<String> imagenesseleccionadas) {
        this.imagenesseleccionadas = imagenesseleccionadas;
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UploadedFile getImagenACargar() {
        return imagenACargar;
    }

    public void setImagenACargar(UploadedFile imagenACargar) {
        this.imagenACargar = imagenACargar;
    }


    public List<Imagenes> getLista() {
            return lista;      
    }

    public String getOpcionSeleccionada() {
        return opcionSeleccionada;
    }

    public void setOpcionSeleccionada(String opcionSeleccionada) {
        this.opcionSeleccionada = opcionSeleccionada;
    }

    public void setLista(List<Imagenes> lista) {
        this.lista = lista;
    }
    
    public OpcionesBean() {
    }
    
    public void cargarImagenes(){
            BigDecimal b=new BigDecimal(BigInteger.ONE);
            this.lista= this.imagenesFacade.obtenerImagenesPorTipoYUsuarioID(opcionSeleccionada, b);
            if(!usuarioSession.usuarioActual.getIdusuario().equals(b))
                this.lista.addAll(this.imagenesFacade.obtenerImagenesPorTipoYUsuarioID(opcionSeleccionada, usuarioSession.usuarioActual.getIdusuario()));
       /*     Imagenes i=new Imagenes();
            i.setDescripcion("Añadir imagenes");
            i.setDireccion("images/anadir.png");
            this.lista.add(0,i);*/         
    }
    
    public String imagenesSeleccionadasss(){
        
        String img= FacesContext.getCurrentInstance().
		getExternalContext().getRequestParameterMap().get("imagenes");
        List<String> myList = new ArrayList<>(Arrays.asList(img.split(" ")));
        return "juegoObjetos.xhtml";
        
    }
    
    public void guardarImagen() throws IOException{
        String imagenDir;
        Imagenes i;
        
        if(this.imagenACargar!=null){       
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            String path = externalContext.getRealPath("/");
            path=path.substring(0, path.indexOf("dist"))+"ProyectoWeb-war/web/images/"+imagenACargar.getFileName();
            File fichero = new File(path);
            FileUtils.copyInputStreamToFile(imagenACargar.getInputstream(), fichero);
        
            imagenDir=("images/"+imagenACargar.getFileName());
            
            i=new Imagenes();
            i.setDescripcion("Imagen agregada por el usuario");
            i.setDireccion(imagenDir);
            i.setIdusuario(usuarioSession.usuarioActual);
            i.setIdtipo(this.tipoFacade.obtenerTipoPorNombre(this.opcionSeleccionada));        
            BigDecimal id=new BigDecimal(0);
            i.setIdimagen(id);
            this.imagenesFacade.create(i);
            this.lista.add(i);
            imagenDir="";
            imagenACargar=null;
        }
        
    }

    public boolean esFondo ()
    {
        return opcionSeleccionada.equals("Fondo");
    
    }        
    
    
    @PostConstruct
    public void cargarPorDefecto(){
      //  this.lista=this.fondosFacade.obtenerTodosNombresFondos();
        this.lista=new ArrayList<>();
        this.imagenesseleccionadas=new ArrayList<>();
        this.opcionSeleccionada="";
    }
    
}
