package com.proyectoweb.bean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.proyectoweb.ejb.JuegoFacade;
import com.proyectoweb.entity.Juego;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author nacho_mc15
 */
@ManagedBean
@RequestScoped
public class JuegosFinalizadosBean {
    @EJB
    private JuegoFacade juegoFacade;

    private List<Juego> juegos;
    private String nombreJuego;
    private int numFases;
    private String nombreUsuario;

    
    public JuegosFinalizadosBean() {
    }

    public List<Juego> getJuegos() {
        return juegos;
    }

    public void setJuegos(List<Juego> juegos) {
        this.juegos = juegos;
    }

    public String getNombreJuego() {
        return nombreJuego;
    }

    public void setNombreJuego(String nombreJuego) {
        this.nombreJuego = nombreJuego;
    }

    public int getNumFases() {
        return numFases;
    }

    public void setNumFases(int numFases) {
        this.numFases = numFases;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String cargarJuegosFinalizados(){
        juegos=this.juegoFacade.obtenerJuegosTerminados();
        return "fasesUsuarios.xhtml";
    }
    
}
