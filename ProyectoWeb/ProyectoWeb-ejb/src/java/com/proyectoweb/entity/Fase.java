/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alex
 */
@Entity
@Table(name = "FASE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fase.findAll", query = "SELECT f FROM Fase f"),
    @NamedQuery(name = "Fase.findByIdfase", query = "SELECT f FROM Fase f WHERE f.idfase = :idfase"),
    @NamedQuery(name = "Fase.findByNombre", query = "SELECT f FROM Fase f WHERE f.nombre = :nombre"),
    @NamedQuery(name = "Fase.findByTiempo", query = "SELECT f FROM Fase f WHERE f.tiempo = :tiempo"),
    @NamedQuery(name = "Fase.findByNintento", query = "SELECT f FROM Fase f WHERE f.nintento = :nintento"),
    @NamedQuery(name = "Fase.findByNobjeto", query = "SELECT f FROM Fase f WHERE f.nobjeto = :nobjeto")})
public class Fase implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDFASE")
    private BigDecimal idfase;
    @Size(max = 20)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "TIEMPO")
    private int tiempo;
    @Column(name = "NINTENTO")
    private int nintento;
    @Column(name = "NOBJETO")
    private int nobjeto;
    @JoinColumn(name = "IDUSUARIO", referencedColumnName = "IDUSUARIO")
    @ManyToOne
    private Usuario idusuario;
    @JoinColumn(name = "IDJUEGO", referencedColumnName = "IDJUEGO")
    @ManyToOne
    private Juego idjuego;
    @JoinColumn(name = "IDFONDO", referencedColumnName = "IDIMAGEN")
    @ManyToOne
    private Imagenes idfondo;
    @JoinColumn(name = "IDTIPOCARTA", referencedColumnName = "IDIMAGEN")
    @ManyToOne
    private Imagenes idtipocarta;
    @OneToMany(mappedBy = "idfase")
    private List<Recuadros> recuadrosList;

    public Fase() {
    }

    public Fase(BigDecimal idfase) {
        this.idfase = idfase;
    }

    public BigDecimal getIdfase() {
        return idfase;
    }

    public void setIdfase(BigDecimal idfase) {
        this.idfase = idfase;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getNintento() {
        return nintento;
    }

    public void setNintento(int nintento) {
        this.nintento = nintento;
    }

    public int getNobjeto() {
        return nobjeto;
    }

    public void setNobjeto(int nobjeto) {
        this.nobjeto = nobjeto;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    public Juego getIdjuego() {
        return idjuego;
    }

    public void setIdjuego(Juego idjuego) {
        this.idjuego = idjuego;
    }

    public Imagenes getIdfondo() {
        return idfondo;
    }

    public void setIdfondo(Imagenes idfondo) {
        this.idfondo = idfondo;
    }

    public Imagenes getIdtipocarta() {
        return idtipocarta;
    }

    public void setIdtipocarta(Imagenes idtipocarta) {
        this.idtipocarta = idtipocarta;
    }

    @XmlTransient
    public List<Recuadros> getRecuadrosList() {
        return recuadrosList;
    }

    public void setRecuadrosList(List<Recuadros> recuadrosList) {
        this.recuadrosList = recuadrosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfase != null ? idfase.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fase)) {
            return false;
        }
        Fase other = (Fase) object;
        if ((this.idfase == null && other.idfase != null) || (this.idfase != null && !this.idfase.equals(other.idfase))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectoweb.entity.Fase[ idfase=" + idfase + " ]";
    }
    
}
