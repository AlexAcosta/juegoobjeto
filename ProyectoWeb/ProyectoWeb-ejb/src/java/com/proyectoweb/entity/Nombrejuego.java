/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alex
 */
@Entity
@Table(name = "NOMBREJUEGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nombrejuego.findAll", query = "SELECT n FROM Nombrejuego n"),
    @NamedQuery(name = "Nombrejuego.findByIdnombrejuego", query = "SELECT n FROM Nombrejuego n WHERE n.idnombrejuego = :idnombrejuego"),
    @NamedQuery(name = "Nombrejuego.findByNombre", query = "SELECT n FROM Nombrejuego n WHERE n.nombre = :nombre")})
public class Nombrejuego implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDNOMBREJUEGO")
    private BigDecimal idnombrejuego;
    @Size(max = 10)
    @Column(name = "NOMBRE")
    private String nombre;
    @OneToMany(mappedBy = "idnombre")
    private List<Juego> juegoList;

    public Nombrejuego() {
    }

    public Nombrejuego(BigDecimal idnombrejuego) {
        this.idnombrejuego = idnombrejuego;
    }

    public BigDecimal getIdnombrejuego() {
        return idnombrejuego;
    }

    public void setIdnombrejuego(BigDecimal idnombrejuego) {
        this.idnombrejuego = idnombrejuego;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Juego> getJuegoList() {
        return juegoList;
    }

    public void setJuegoList(List<Juego> juegoList) {
        this.juegoList = juegoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idnombrejuego != null ? idnombrejuego.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nombrejuego)) {
            return false;
        }
        Nombrejuego other = (Nombrejuego) object;
        if ((this.idnombrejuego == null && other.idnombrejuego != null) || (this.idnombrejuego != null && !this.idnombrejuego.equals(other.idnombrejuego))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectoweb.entity.Nombrejuego[ idnombrejuego=" + idnombrejuego + " ]";
    }
    
}
