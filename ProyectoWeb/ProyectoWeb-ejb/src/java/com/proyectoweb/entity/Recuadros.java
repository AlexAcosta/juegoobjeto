/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Alex
 */
@Entity
@Table(name = "RECUADROS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recuadros.findRecuadrosPorUsuarioYjuego", query = "SELECT r FROM Recuadros r WHERE r.idfase.idjuego.idnombre.idnombrejuego = :idjuego AND r.idfase.idusuario.idusuario = :idusuario"),
    @NamedQuery(name = "Recuadros.findAll", query = "SELECT r FROM Recuadros r"),
    @NamedQuery(name = "Recuadros.findByIdrecuadro", query = "SELECT r FROM Recuadros r WHERE r.idrecuadro = :idrecuadro"),
    @NamedQuery(name = "Recuadros.findByOrden", query = "SELECT r FROM Recuadros r WHERE r.orden = :orden")})
public class Recuadros implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDRECUADRO")
    private BigDecimal idrecuadro;
    @Column(name = "ORDEN")
    private BigInteger orden;
    @JoinColumn(name = "IDIMAGENES", referencedColumnName = "IDIMAGEN")
    @ManyToOne
    private Imagenes idimagenes;
    @JoinColumn(name = "IDFASE", referencedColumnName = "IDFASE")
    @ManyToOne
    private Fase idfase;

    public Recuadros() {
    }

    public Recuadros(BigDecimal idrecuadro) {
        this.idrecuadro = idrecuadro;
    }

    public BigDecimal getIdrecuadro() {
        return idrecuadro;
    }

    public void setIdrecuadro(BigDecimal idrecuadro) {
        this.idrecuadro = idrecuadro;
    }

    public BigInteger getOrden() {
        return orden;
    }

    public void setOrden(BigInteger orden) {
        this.orden = orden;
    }

    public Imagenes getIdimagenes() {
        return idimagenes;
    }

    public void setIdimagenes(Imagenes idimagenes) {
        this.idimagenes = idimagenes;
    }

    public Fase getIdfase() {
        return idfase;
    }

    public void setIdfase(Fase idfase) {
        this.idfase = idfase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrecuadro != null ? idrecuadro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recuadros)) {
            return false;
        }
        Recuadros other = (Recuadros) object;
        if ((this.idrecuadro == null && other.idrecuadro != null) || (this.idrecuadro != null && !this.idrecuadro.equals(other.idrecuadro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectoweb.entity.Recuadros[ idrecuadro=" + idrecuadro + " ]";
    }
    
}
