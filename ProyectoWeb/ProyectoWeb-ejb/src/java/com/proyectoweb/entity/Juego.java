/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alex
 */
@Entity
@Table(name = "JUEGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Juego.findAll", query = "SELECT j FROM Juego j"),
    @NamedQuery(name = "Juego.findByIdjuego", query = "SELECT j FROM Juego j WHERE j.idjuego = :idjuego"),
    @NamedQuery(name = "Juego.findByPista", query = "SELECT j FROM Juego j WHERE j.pista = :pista"),
    @NamedQuery(name = "Juego.findByAccionobjeto", query = "SELECT j FROM Juego j WHERE j.accionobjeto = :accionobjeto"),
    @NamedQuery(name = "Juego.findByTerminado", query = "SELECT j FROM Juego j WHERE j.terminado = :terminado"),
    @NamedQuery(name = "Juego.findByDireccionimagen", query = "SELECT j FROM Juego j WHERE j.direccionimagen = :direccionimagen")})
public class Juego implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDJUEGO")
    private BigDecimal idjuego;
    @Column(name = "PISTA")
    private int pista;
    @Column(name = "ACCIONOBJETO")
    private int accionobjeto;
    @Column(name = "TERMINADO")
    private int terminado;
    @Size(max = 40)
    @Column(name = "DIRECCIONIMAGEN")
    private String direccionimagen;
    @OneToMany(mappedBy = "idjuego")
    private List<Fase> faseList;
    @JoinColumn(name = "IDUSUARIO", referencedColumnName = "IDUSUARIO")
    @ManyToOne(optional = false)
    private Usuario idusuario;
    @JoinColumn(name = "IDNOMBRE", referencedColumnName = "IDNOMBREJUEGO")
    @ManyToOne
    private Nombrejuego idnombre;

    public Juego() {
    }

    public Juego(BigDecimal idjuego) {
        this.idjuego = idjuego;
    }

    public BigDecimal getIdjuego() {
        return idjuego;
    }

    public void setIdjuego(BigDecimal idjuego) {
        this.idjuego = idjuego;
    }

    public int getPista() {
        return pista;
    }

    public void setPista(int pista) {
        this.pista = pista;
    }

    public int getAccionobjeto() {
        return accionobjeto;
    }

    public void setAccionobjeto(int accionobjeto) {
        this.accionobjeto = accionobjeto;
    }

    public int getTerminado() {
        return terminado;
    }

    public void setTerminado(int terminado) {
        this.terminado = terminado;
    }

    public String getDireccionimagen() {
        return direccionimagen;
    }

    public void setDireccionimagen(String direccionimagen) {
        this.direccionimagen = direccionimagen;
    }

    @XmlTransient
    public List<Fase> getFaseList() {
        return faseList;
    }

    public void setFaseList(List<Fase> faseList) {
        this.faseList = faseList;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    public Nombrejuego getIdnombre() {
        return idnombre;
    }

    public void setIdnombre(Nombrejuego idnombre) {
        this.idnombre = idnombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idjuego != null ? idjuego.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Juego)) {
            return false;
        }
        Juego other = (Juego) object;
        if ((this.idjuego == null && other.idjuego != null) || (this.idjuego != null && !this.idjuego.equals(other.idjuego))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.proyectoweb.entity.Juego[ idjuego=" + idjuego + " ]";
    }
    
}
