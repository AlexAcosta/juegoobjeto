/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.ejb;

import com.proyectoweb.entity.Recuadros;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class RecuadrosFacade extends AbstractFacade<Recuadros> {
    @PersistenceContext(unitName = "ProyectoWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RecuadrosFacade() {
        super(Recuadros.class);
    }
     public Recuadros obtenerRecuadro(BigDecimal fase, BigInteger orden){
        List<Recuadros> listaRecuadros = em.createQuery("SELECT r FROM Recuadros r WHERE r.orden = :orden and r.idfase.idfase = :fase")     
            .setParameter("orden", orden) 
            .setParameter("fase", fase) 
            .getResultList();  
        for(Recuadros f : listaRecuadros){
            return f;
        }
        return null;
    }
     
         public List<Recuadros> obtenerRecuadrosFase(BigDecimal fase){
         return em.createQuery("SELECT r FROM Recuadros r WHERE r.idfase.idfase = :fase ORDER BY r.orden")    
            .setParameter("fase", fase)   
            .getResultList();  
    }
         
         public List<Recuadros> findRecuadrosPorUsuarioYjuego(int idjuego, int idusuario){
            return em.createNamedQuery("Recuadros.findRecuadrosPorUsuarioYjuego").setParameter("idjuego", idjuego).setParameter("idusuario", idusuario).getResultList();
         }
}
