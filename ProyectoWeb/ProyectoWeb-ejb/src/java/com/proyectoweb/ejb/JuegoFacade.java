/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.ejb;

import com.proyectoweb.entity.Juego;
import com.proyectoweb.entity.Nombrejuego;
import com.proyectoweb.entity.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class JuegoFacade extends AbstractFacade<Juego> {
    @PersistenceContext(unitName = "ProyectoWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public JuegoFacade() {
        super(Juego.class);
    }
     public Juego obtenerJuegoPorUsuarioYJuego(Usuario user, Nombrejuego juego){
        List<Juego> listaJuego= em.createQuery("SELECT j FROM Juego j WHERE j.idusuario.idusuario = :user and j.idnombre.idnombrejuego = :juego")
            .setParameter("user", user.getIdusuario())    
            .setParameter("juego", juego.getIdnombrejuego())  
            .getResultList(); 
        for(Juego j : listaJuego){
            return j;
        }
        return null;
    }
     public List<Juego> obtenerJuegosTerminados(){
         return em.createQuery("SELECT j FROM Juego j WHERE j.terminado = 1 ")    
            .getResultList();  
    }
}
