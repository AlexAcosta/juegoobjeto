/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.ejb;

import com.proyectoweb.entity.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "ProyectoWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
      public Usuario findByLoginAndPasswd(String login, String passwd) {
        List<Usuario> resultado = em.createQuery("SELECT u From Usuario u WHERE u.login = :login and u.pass = :passwd")
                .setParameter("login", login)
                .setParameter("passwd", passwd)
                .getResultList();
        if (resultado != null && resultado.size() > 0) {
            return resultado.get(0);
        } else {
            return null;
        }
    }

    public Usuario findByLogin(String login) {
        List<Usuario> resultado = em.createQuery("SELECT u From Usuario u WHERE u.login = :login")
                .setParameter("login", login)
                .getResultList();
        if (resultado != null && resultado.size() > 0) {
            System.out.print(resultado.size());
            return resultado.get(0);
        } else {
            return null;
        }
    }
}
