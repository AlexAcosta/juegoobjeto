/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.ejb;

import com.proyectoweb.entity.Imagenes;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class ImagenesFacade extends AbstractFacade<Imagenes> {
    @PersistenceContext(unitName = "ProyectoWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ImagenesFacade() {
        super(Imagenes.class);
    }
    public List<Imagenes> obtenerImagenesPorTipoYUsuarioID(String tipo,BigDecimal user){
        return em.createQuery("SELECT i FROM Imagenes i WHERE i.idtipo.nombre = :tipo and i.idusuario.idusuario = :user")
            .setParameter("tipo", tipo)  
            .setParameter("user", user)
            .getResultList();                
    }
    
    public Imagenes obtenerImagenesPorID(BigDecimal id){
        return (Imagenes) em.createQuery("SELECT i FROM Imagenes i WHERE i.idimagen = :id")
            .setParameter("id", id)               
            .getSingleResult();
    }
}
