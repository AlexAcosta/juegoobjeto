/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.ejb;

import com.proyectoweb.entity.Fase;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class FaseFacade extends AbstractFacade<Fase> {
    @PersistenceContext(unitName = "ProyectoWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FaseFacade() {
        super(Fase.class);
    }
    public Fase obtenerFase(BigDecimal juego, String nombre, BigDecimal user){
        List<Fase> listaFases = em.createQuery("SELECT f FROM Fase f WHERE f.nombre = :nombre and f.idjuego.idjuego = :juego and f.idusuario.idusuario = :user")    
            .setParameter("juego", juego)  
            .setParameter("nombre", nombre) 
            .setParameter("user", user) 
            .getResultList();  
        for(Fase f : listaFases){
            return f;
        }
        return null;
    }
    
    public List<Fase> obtenerFasesUsuario(BigDecimal juego, BigDecimal user){
         return em.createQuery("SELECT f FROM Fase f WHERE f.idjuego.idjuego = :juego and f.idusuario.idusuario = :user ORDER BY f.nombre")    
            .setParameter("juego", juego)   
            .setParameter("user", user) 
            .getResultList();  
    }
}
