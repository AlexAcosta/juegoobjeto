/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.ejb;

import com.proyectoweb.entity.Nombrejuego;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alex
 */
@Stateless
public class NombrejuegoFacade extends AbstractFacade<Nombrejuego> {
    @PersistenceContext(unitName = "ProyectoWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NombrejuegoFacade() {
        super(Nombrejuego.class);
    }
     public Nombrejuego obtenerNombrejuegoPorID(String juego){
        List<Nombrejuego> listaNombres = em.createQuery("SELECT j FROM Nombrejuego j WHERE j.nombre LIKE :juego")    
            .setParameter("juego", juego)  
            .getResultList();  
        for(Nombrejuego nj : listaNombres){
            return nj;
        }
        return null;
    }
}
