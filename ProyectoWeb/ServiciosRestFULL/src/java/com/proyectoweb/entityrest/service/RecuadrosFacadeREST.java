/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.entityrest.service;

import com.proyectoweb.ejb.RecuadrosFacade;
import com.proyectoweb.entity.Recuadros;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Alex
 */
@Stateless
@Path("com.proyectoweb.entityrest.recuadros")
public class RecuadrosFacadeREST{
    
     @EJB
     private RecuadrosFacade recuadrosFacade;
    
     public RecuadrosFacadeREST() {
     }

//    @POST
//    @Consumes({"application/xml", "application/json"})
//    public void create(Recuadros entity) {
//        recuadrosFacade.create(entity);
//    }
//
//    @PUT
//    @Path("{id}")
//    @Consumes({"application/xml", "application/json"})
//    public void edit(@PathParam("id") BigDecimal id, Recuadros entity) {
//       recuadrosFacade.edit(entity);
//    }
//
//    @DELETE
//    @Path("{id}")
//    public void remove(@PathParam("id") BigDecimal id) {
//        recuadrosFacade.remove(recuadrosFacade.find(id));
//    }
//
//    @GET
//    @Path("{id}")
//    @Produces({"application/xml", "application/json"})
//    public Recuadros find(@PathParam("id") BigDecimal id) {
//        return recuadrosFacade.find(id);
//    }
//
//    @GET
//    @Produces({"application/json"})
//    public List<Recuadros> findAll() {
//        return recuadrosFacade.findAll();
//    }
//
//    @GET
//    @Path("{from}/{to}")
//    @Produces({"application/xml", "application/json"})
//    public List<Recuadros> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
//        return recuadrosFacade.findRange(new int[]{from, to});
//    }
//
//    @GET
//    @Path("count")
//    @Produces("text/plain")
//    public String countREST() {
//        return String.valueOf(recuadrosFacade.count());
//    }

    @GET
    @Path("idjuego={idjuego}&idusuario={idusuario}")
    @Produces ({"application/json"})
    public List<Recuadros> encontrarRecuadrosPorUsuarioYJuego (@PathParam("idjuego") int idjuego,@PathParam("idusuario") int idusuario){
       
        return recuadrosFacade.findRecuadrosPorUsuarioYjuego(idjuego, idusuario);
        
    }
}
