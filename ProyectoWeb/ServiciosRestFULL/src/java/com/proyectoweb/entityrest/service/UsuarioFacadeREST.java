/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.proyectoweb.entityrest.service;

import com.proyectoweb.ejb.UsuarioFacade;
import com.proyectoweb.entity.Usuario;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 *
 * @author Alex
 */
@Stateless
@Path("com.proyectoweb.entityrest.usuario")
public class UsuarioFacadeREST{
    
    @EJB
    private UsuarioFacade usuarioFacade;
    
    public UsuarioFacadeREST() {
    }

//    @POST
//    @Consumes({"application/xml", "application/json"})
//    public void create(Usuario entity) {
//        usuarioFacade.create(entity);
//    }
//
//    @PUT
//    @Path("{id}")
//    @Consumes({"application/xml", "application/json"})
//    public void edit(@PathParam("id") BigDecimal id, Usuario entity) {
//        usuarioFacade.edit(entity);
//    }
//
//    @DELETE
//    @Path("{id}")
//    public void remove(@PathParam("id") BigDecimal id) {
//       usuarioFacade.remove(usuarioFacade.find(id));
//    }
//
//    @GET
//    @Path("{id}")
//    @Produces({"application/xml", "application/json"})
//    public Usuario find(@PathParam("id") BigDecimal id) {
//        return usuarioFacade.find(id);
//    }
//
//    @GET
//    @Produces({"application/xml", "application/json"})
//    public List<Usuario> findAll() {
//        return usuarioFacade.findAll();
//    }
//
//    @GET
//    @Path("{from}/{to}")
//    @Produces({"application/xml", "application/json"})
//    public List<Usuario> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
//        return usuarioFacade.findRange(new int[]{from, to});
//    }
//
//    @GET
//    @Path("count")
//    @Produces("text/plain")
//    public String countREST() {
//        return String.valueOf(usuarioFacade.count());
//    }
    
    @GET
    @Path("/comprobar")
    @Produces({"application/json"})
    public Usuario findByLoginAndPasswd(@QueryParam("login") String login,@QueryParam("pass") String passwd) {
       
            return usuarioFacade.findByLoginAndPasswd(login, passwd);
        
    }

 }
