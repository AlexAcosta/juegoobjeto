DROP TABLE Usuario CASCADE CONSTRAINT;
DROP TABLE NombreJuego CASCADE CONSTRAINT;
DROP TABLE Juego CASCADE CONSTRAINT;
DROP TABLE Imagenes CASCADE CONSTRAINT;
DROP TABLE Fase CASCADE CONSTRAINT;
DROP TABLE FaseImagen CASCADE CONSTRAINT;
DROP TABLE Tipo CASCADE CONSTRAINT;

DROP sequence usuarioseq; 
DROP sequence juegoseq; 
DROP sequence imagenesseq; 
DROP sequence faseseq; 
DROP sequence tiposeq; 



CREATE TABLE Usuario
(idUsuario NUMBER,
login VARCHAR2(10),
pass VARCHAR2(30),
nombre VARCHAR2(20),
apellido VARCHAR2(20),
CONSTRAINT usuario_id_pk PRIMARY KEY (idUsuario),
CONSTRAINT usuario_login_uk UNIQUE (login));

CREATE TABLE NombreJuego
(idNombreJuego NUMBER,
Nombre VARCHAR2(10),
CONSTRAINT nombrejuego_id_pk PRIMARY KEY (idNombreJuego));

CREATE TABLE Juego
(idJuego NUMBER,
idNombre NUMBER,
pista NUMBER,
accionobjeto NUMBER,
CONSTRAINT juego_id_pk PRIMARY KEY (idJuego),
CONSTRAINT juego_id_fk FOREIGN KEY (idNombre) REFERENCES NombreJuego(idNombreJuego));


CREATE TABLE Tipo
(idTipo NUMBER,
nombre VARCHAR2(30),
CONSTRAINT tipo_id_pk PRIMARY KEY (idTipo)
);

CREATE TABLE Imagenes
(idImagen NUMBER,
idUsuario NUMBER,
idTipo NUMBER,
direccion VARCHAR2(20),
descripcion VARCHAR2(50),
CONSTRAINT imagen_id_pk PRIMARY KEY (idImagen),
CONSTRAINT imagen_usu_fk FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
CONSTRAINT imagen_tipo_fk FOREIGN KEY (idTipo) REFERENCES Tipo(idTipo)
);




CREATE TABLE Fase
(idFase NUMBER,
nombre VARCHAR2(20),
tiempo NUMBER,
nintento NUMBER,
movimiento NUMBER,
idUsuario NUMBER,
CONSTRAINT Fase_id_pk PRIMARY KEY (idFase),
CONSTRAINT Fase_idUsuario_fk FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)
);


CREATE TABLE FaseImagen
(idFase NUMBER,
idImagenes NUMBER,
bloqueX NUMBER,
bloqueY NUMBER,
CONSTRAINT FaseImagen_id_pk PRIMARY KEY(idFase,idImagenes),
CONSTRAINT FaseImagen_idfase_fk FOREIGN KEY(idFase) REFERENCES Fase(idFase),
CONSTRAINT FaseImagen_idobjeto_fk FOREIGN KEY(idImagenes) REFERENCES Imagenes(idImagen)
);


create sequence usuarioseq 
start with 1 
increment by 1 
nomaxvalue;
create sequence juegoseq 
start with 1 
increment by 1 
nomaxvalue;
create sequence imagenesseq 
start with 1 
increment by 1 
nomaxvalue;
create sequence faseseq 
start with 1 
increment by 1 
nomaxvalue;

create sequence tiposeq 
start with 1 
increment by 1 
nomaxvalue;


CREATE OR REPLACE TRIGGER TRIG_USUARIOS
BEFORE INSERT ON Usuario
FOR EACH ROW
BEGIN
SELECT usuarioseq.NEXTVAL INTO :NEW.idUsuario  FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER TRIG_TIPO
BEFORE INSERT ON Tipo
FOR EACH ROW
BEGIN
SELECT tiposeq.NEXTVAL INTO :NEW.idTipo  FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER TRIG_JUEGOS
BEFORE INSERT ON Juego
FOR EACH ROW
BEGIN
SELECT juegoseq.NEXTVAL INTO :NEW.idJuego FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER TRIG_FASES
BEFORE INSERT ON Fase
FOR EACH ROW
BEGIN
SELECT faseseq.NEXTVAL INTO :NEW.idFase FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER TRIG_IMAGENES
BEFORE INSERT ON Imagenes
FOR EACH ROW
BEGIN
SELECT imagenesseq.NEXTVAL INTO :NEW.idImagen FROM DUAL;
END;
/

